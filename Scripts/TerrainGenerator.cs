﻿using UnityEngine;

[ExecuteInEditMode]
public class TerrainGenerator : MonoBehaviour
{
    public int Width, Length, Height, Scale;
    public float Accuracy;
    
    private Terrain terrain;
    private TerrainData data;
    private float[,] heights;

    public void GenerateTerrain()
    {
        Init();

        data.size = new Vector3(Width, Height, Length);
        data.heightmapResolution = Width + 1;

        for (int x = 0; x < Width; x++)
        {
            for (int z = 0; z < Length; z++)
            {
                float xValue = (float)x / Width * Scale;
                float zValue = (float)z / Length * Scale;
                float heightValue = Mathf.PerlinNoise(xValue, zValue);

                heights[x, z] = heightValue;

                heights[x, 0] = 0.0f;
                heights[0, z] = 0.0f;
                heights[x, Length - 1] = 0.0f;
                heights[Width - 1, z] = 0.0f;
            }
        }

        NormalizeTerrain(Accuracy);

        data.SetHeights(0, 0, heights);
    }

    public void ResetTerrain()
    {
        Init();

        Width = 0;
        Length = 0;
        Height = 0;
        Scale = 0;
        Accuracy = 0;

        for (int x = 0; x < Width; x++)
        {
            for (int z = 0; z < Length; z++)
            {
                heights[x, z] = 0.0f;
            }
        }

        data.SetHeights(0, 0, heights);
    }
    
    private void NormalizeTerrain(float accuracy)
    {
        for (int x = 0; x < Width; x++)
        {
            for (int z = 0; z < Length; z++)
            {
                if (heights[x, z] <= accuracy)
                    heights[x, z] = 0.0f;
                else
                    heights[x, z] = Height;
            }
        }
    }

    private void Init()
    {
        terrain = GetComponent<Terrain>();
        data = terrain.terrainData;
        heights = new float[Width, Length];
    }
}
