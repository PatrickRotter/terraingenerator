﻿using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
[CustomEditor(typeof(TerrainGenerator))]

public class GeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TerrainGenerator generator = (TerrainGenerator)target;

        if (GUILayout.Button("Generate Terrain"))
            generator.GenerateTerrain();

        if (GUILayout.Button("Reset"))
            generator.ResetTerrain();
    }
}
#endif
